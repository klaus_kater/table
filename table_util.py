# -*- coding: utf-8 -*-

class PPTable(object):
    """
        Класс для вывода таблиц в консоль
    """
    # ╔══╦══╗
    # ║  ║  ║
    # ╠══╬══╣
    # ║  ║  ║
    # ╚══╩══╝

    # ▌▐
    # │ ┤ ╡ ╢ ╖ ╕ ╣ ║ ╗ ╝ ╜ ╛ ┐ └ ┴ ┬
    #
    # ├ ─ ┼ ╞ ╟ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╧ ╨ ╤ ╥ ╙ ╘ ╒ ╓ ╫ ╪ ┘ ┌
    def __init__(self):
        self.table = self.Table()
        self.width_screen = 80 # по умолчанию ширина экрана 80, в случае чего можно переопределить

    def __repr__(self):
        # Печатью занимается исключительно главный объект, на случай мультитаблицы

        # Первичный рендер ячеек, переводим объекты в ячейках, в список строк ячейки
        self.table.templater()
        self.table.calc_sizes()

        # у нас есть расчитанные габариты ячеек. надо добить текст в ячейках до нужной ширины,
        # хотя это можно сделать позже

        self.table.convert_tostrings()

        return self.table.list_strings
        # for st in self.table.list_strings:
        #     print st

    def set_template(self, func_template):
        """ Устанавливается функция для отрисовки одной ячейки. От входной функции требуется список со стоками"""
        self.table.template = func_template

    def print_table(self):
        # печать
        for str in self.__repr__():
            print str
        print "\n"

    def add_header(self, name):
        """
            Добавляем шапку
        :param name: Строка для шапки
            # ╔test═╗
            # ║  ║  ║
            # ╠══╬══╣
            # ║  ║  ║
            # ╚══╩══╝
        """
        self.table.add_header(name)

    def add_line(self, y, list_data):
        for data in list_data:
            self.table.add_cell(list_data.index(data), y, data)

    def add_cell(self, x, y, obj_data, height_rows = 1, width_cols = 1, type='normal'):
        self.table.add_cell(x, y, obj_data, type=type, height_rows=height_rows, width_cols=width_cols)

    class Cell(object):
        obj_data = None

        def __init__(self):
            self.width = 0
            self.height = 0
            self.width_cols = 1
            self.height_rows = 1
            self.obj_data = ''
            self.parent = None
            self.list_str = []
            # self.type = 'normal'

        def templater(self):
            self.list_str = self.parent.template(self.obj_data or '')
            self.width = max([len(str_one) for str_one in self.list_str])
            self.height = len(self.list_str)

        def __repr__(self):
            return 'w %d h %d wc %d hr %d' % (self.width, self.height, self.width_cols, self.height_rows)

    class Table(Cell):
        def __init__(self):
            def templat_func(text):
                """
                    Дефолтный отрисовщик
                :param text: входная строка
                :return: список строк, для поочередного вывода в ячейке
                """
                return [str.lstrip() for str in text.split(',')]

            super(self.__class__, self).__init__()
            # в строка хранятся колонки
            self.table = {}  # rows
            self.table[0] = {}  # cols
            self.num_rows = 0
            self.num_cols = 0
            self.list_width_cols = []
            self.list_height_rows = []
            self.massaraksh_table = {} # Хранилище таблицы в обратном порядке (строки в столбцах)
            self.template = templat_func
            self.name = ''

        def _fill_to_up(self):
            # дозаполняем ячейки которые были незаполнены
            for row in range(self.num_rows):
                if not self.table.get(row):
                    self.table[row] = {}
                for col in range(self.num_cols):
                    if not self.table[row].get(col):
                        self.add_cell(col, row, None)

        def templater(self):

            def calc_width(cell):
                """
                    Вычисляет ширины в ряду
                """
                max_index = max(cell.keys())
                width_cols = cell.get(max_index).width_cols if cell.get(max_index) else 0
                return width_cols * (max_index+1)


            # этому тут тож не место.. это инициализация данных таблицы, когда она уже заполнена, но до расчетов..
            self.num_cols = max([calc_width(row) for kr, row in self.table.iteritems()])
            # собираем из ячеек в строках - ячейки в столбцах
            for kr, kc, cell in [(kr, kc, cell) for kr, row in self.table.iteritems() for kc, cell in row.iteritems()]:
                if not self.massaraksh_table.get(kc):
                    self.massaraksh_table[kc] = {}
                self.massaraksh_table[kc][kr] = cell
            self.num_rows = max([calc_width(row) for kr, row in self.massaraksh_table.iteritems()])

            # self._fill_to_up()  # на случай если не все ячейки заполнены
            # Проходимся шаблоном по всем ячейкам
            for row in self.table.keys():
                for col in self.table[row].keys():
                    self.table[row][col].templater()
                    # Решаем пробелму с юникодом
                    self.table[row][col].list_str = [u''+st for st in self.table[row][col].list_str]

            # self.list_str # Пока не понятно, нужен ли этот параметр..

        def calc_sizes(self):
            # расчет широт (да и высот) таблицы.
            # ╔══╦══╗
            # ║  ║  ║
            # ╠══╩══╣
            # ║     ║
            # ╚═════╝
            # итеративно собираем колонки, берем список ячеек у которых правая граница совпадает с нужной
            # сделать список ячеек по столбцам, потом пройтись по списку, посчитать ширины

            # имеем список столбцов, собственно их и проходим, в обратном порядке
            self.list_width_cols.append(1) # костыль, иначе много где приходится обрабатывать пустой массив
            for i in range(self.num_cols-1, -1, -1):
                tmp_width = [0]
                if not i in self.massaraksh_table.keys():
                    self.list_width_cols.append(self.list_width_cols[-1:][0]+1)
                    continue
                for key_cell, cell in self.massaraksh_table[i].iteritems():
                    # сумма ширины ячейки, и ширин предыдущих ячеек (с учетом что ячейка может быть шире одной)
                    tmp_width_cell = cell.width + self.list_width_cols[-1*cell.width_cols]
                    tmp_width.append(tmp_width_cell)

                self.list_width_cols.append(max(tmp_width or [0])+1)
            # в списке ширин - ширина ячейки со всеми предыдущими, нормируем
            self.list_width_cols = [l-r-1 for l,r in zip(self.list_width_cols[1:],self.list_width_cols[:-1])]
            self.list_width_cols.reverse()

            self.list_height_rows.append(1)
            for i in range(self.num_rows-1, -1, -1):
                tmp_height = [0]
                if not i in self.table.keys():
                    self.list_height_rows.append(self.list_height_rows[-1:][0]+1)
                    continue
                for key_cell, cell in self.table[i].iteritems():
                    tmp_height_cell = cell.height + self.list_height_rows[-1*cell.height_rows]
                    tmp_height.append(tmp_height_cell)

                self.list_height_rows.append(max(tmp_height or [0])+1)
            self.list_height_rows = [l-r-1 for l,r in zip(self.list_height_rows[1:],self.list_height_rows[:-1])]
            self.list_height_rows.reverse()

            # заносим реальные ширины в ячейки

            for kr, row in self.table.iteritems():
                for kc, cell in row.iteritems():
                    cell.width = sum(self.list_width_cols[kc:kc+cell.width_cols]) + cell.width_cols - 1
                    cell.height = sum(self.list_height_rows[kr:kr+cell.height_rows]) + cell.height_rows - 1
                    # print kr, kc, cell.width, cell.height

        # def optimize_width(self):
        #     """Функция растягивает таблицу если ширина задана больше чем надо."""
        def convert_tostrings(self):
            # Переводим из вида таблицы, в набор строк для печати
            list_strings = []
            #
            # перебираем строки таблицы
            def horizontal_line(list_width_cols, pos):
                """
                :param list_width_cols: Список, с ширинами колонок
                :param pos: позиция линии (0 - первая, 1- средняя, 2 - последняя)
                :return:
                """
                map_spec = {
                    'top':    {'start': u"╔", 'center': u"╦", 'end': u"╗"},
                    'middle': {'start': u"╠", 'center': u"╬", 'end': u"╣"},
                    'bottom': {'start': u"╚", 'center': u"╩", 'end': u"╝"},
                }
                return map_spec[pos]['start'] + map_spec[pos]['center'].join(
                    [u"═" * width for width in list_width_cols]) + map_spec[pos]['end']

            table_data = self.table
            # для начала просто заполняем пустую таблицу
            # ╔══╦══╗
            list_strings.append(horizontal_line(self.list_width_cols, 'top'))
            for row in range(self.num_rows):
                # for cell in xrange(self.num_cols):
                one_line = u'║%s║' % u'║'.join([' '*width for width in self.list_width_cols])
                for x in range(self.list_height_rows[row]):
                    list_strings.append(one_line)
                # ╠══╬══╣
                # ╚══╩══╝
                list_strings.append(horizontal_line(self.list_width_cols, 'middle' if row < self.num_rows - 1 else 'bottom'))
            # заменяем пустые ячейки данными

            def get_r_s(side, symbol):
                custom_cell_map = {
                    'top':    {
                        u"╦":u"═", u"╬":u"╩", u"╪":u"╧", u"╫":u"╨", u"╤":u"═", u"┬":u"─", u"┼":u"┴", u"╥":u"─"},
                    'bottom': {
                        u"╩":u"═", u"╬":u"╦", u"╪":u"╤", u"╫":u"╥", u"╧":u"═", u"┴":u"─", u"┼":u"┬", u"╨":u"─"},
                    'left':   {
                        u"╠":u"║", u"╬":u"╣", u"├":u"│", u"┼":u"┤", u"╞":u"│", u"╟":u"║", u"╫":u"╢", u"╪":u"╡"},
                    'right':  {
                        u"╣":u"║", u"╬":u"╠", u"┤":u"│", u"╡":u"│", u"╢":u"║", u"┼":u"├", u"╫":u"╟", u"╪":u"╞"}
                }

                return custom_cell_map[side].get(symbol, symbol)

            for kr, row in table_data.iteritems():
                for kc, cell in row.iteritems():
                    # вычисляем координаты ячейки
                    x = sum([width+1 for width in self.list_width_cols[:kc]])+1
                    y = sum([height+1 for height in self.list_height_rows[:kr]])+1

                    # заодно обходим контур ячейки, заменяем ответвления линий, на ровные ╬ -> ╣
                    top_str = list_strings[y-1][x:x+cell.width]
                    top_str = [get_r_s('top', ms) for ms in list(top_str)]
                    list_strings[y-1] = list_strings[y-1][:x]+''.join(top_str)+list_strings[y-1][x+cell.width:]

                    for linenum_cell in range(cell.height):
                        line_str = cell.list_str[linenum_cell] if len(cell.list_str) > linenum_cell else ''
                        list_strings[y] = list_strings[y][:x-1]+\
                                            get_r_s('left', list_strings[y][x-1])+\
                                            line_str+u' '*(cell.width - len(line_str))+\
                                            get_r_s('right', list_strings[y][x+cell.width])+\
                                            list_strings[y][x+1+cell.width:]
                        y += 1

                    bot_str = list_strings[y][x:x+cell.width]
                    bot_str = [get_r_s('bottom', ms) for ms in list(bot_str)]
                    list_strings[y] = list_strings[y][:x]+''.join(bot_str)+list_strings[y][x+cell.width:]

                    # print kr, kc, x, y

            self.list_strings = list_strings


        def add_cell(self, x, y, obj_data, type='normal', height_rows=1, width_cols=1):
            """
                Обработка добавления данных в ячейку.
            :param x: координата
            :param y: координата
            :param obj_data: объект в ячейке
            """

            # Заполняем ячейку в таблице
            if not self.table.get(y):
                self.table[y] = {}

            if self.table[y].get(x):  # ячейка занята. Надо ошибку выдавать..
                return False

            cell = PPTable.Cell()
            cell.parent = self
            cell.obj_data = obj_data
            cell.height_rows = height_rows
            cell.width_cols = width_cols
            cell.type = type
            self.table[y][x] = cell

        def add_header(self, name):
            self.name = name

    class MultiTable(Table):
        def __init__(self, table):
            self.table = table

if __name__ == "__main__":
    # проблемы с руским текстом..
    table = PPTable()
    table.add_header('Test header')
    table.add_cell(0, 0, 'tt, bbbb')
    table.add_cell(5, 4, '111, 2222')
    table.add_cell(1, 2, '111')
    table.add_cell(1, 3, 'test')
    table.add_cell(2, 4, u'абракадабра')
    table.add_cell(1, 1, 'testsadfsdf', width_cols=2)
    table.add_cell(2, 2, 'tt, 22, 1322', height_rows=2)
    # Вложенная таблица.
    # new_table = PPTable()
    # new_table.add_cell(0, 0, 'tt, bbbb')
    # table.add_cell(1,2, new_table)

    table.print_table()

